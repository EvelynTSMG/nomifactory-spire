package Nomifactory.relics;

import com.megacrit.cardcrawl.actions.utility.ScryAction;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.atb;

public class ElectricProspectorsScanner extends BaseRelic {
    public final static String NAME = ElectricProspectorsScanner.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.STARTER;
    public final static LandingSound SOUND = LandingSound.FLAT;
    public final static int EFFECT = 3;

    public ElectricProspectorsScanner() {
        super(NAME, TIER, SOUND);
    }

    @Override
    public void atBattleStartPreDraw() {
        atb(new ScryAction(EFFECT));
    }
}