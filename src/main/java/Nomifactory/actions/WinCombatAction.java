package Nomifactory.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.rooms.AbstractRoom;

import static Nomifactory.util.Wiz.player;

public class WinCombatAction extends AbstractGameAction {
    @Override
    public void update() {
        if (AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT) {
            player().hideHealthBar();
            player().isEscaping = true;
            AbstractDungeon.overlayMenu.endTurnButton.disable();
            player().escapeTimer = 2.5F;
        }

        isDone = true;
    }
}
