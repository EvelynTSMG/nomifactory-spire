package Nomifactory.actions;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.EmptyDeckShuffleAction;
import com.megacrit.cardcrawl.actions.common.ShuffleAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;

import java.util.function.Predicate;

import static Nomifactory.util.Wiz.cards_in_groups;
import static Nomifactory.util.Wiz.player;

public class DrawFilteredCardAction extends AbstractGameAction {
    public final Predicate<AbstractCard> filter;
    public int amount;

    public DrawFilteredCardAction(Predicate<AbstractCard> filter, int amount) {
        this.filter = filter;
        this.amount = amount;
    }

    @Override
    public void update() {
        for (AbstractCard card : cards_in_groups(CardGroup.CardGroupType.DRAW_PILE)) {
            if (filter.test(card)) {
                player().drawPile.moveToHand(card, player().drawPile);

                amount--;
                if (amount <= 0) break;
            }
        }

        isDone = true;
    }
}
