package Nomifactory.powers;

import Nomifactory.NomifactoryCharacter;
import Nomifactory.powers.BasePower;

import com.evacipated.cardcrawl.mod.stslib.powers.interfaces.NonStackablePower;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.RemoveSpecificPowerAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class DamageNextTurnPower extends BasePower implements NonStackablePower {
    public final static String NAME = DamageNextTurnPower.class.getSimpleName();
    public final static PowerType TYPE = PowerType.BUFF;
    public final static boolean TURN_BASED = false;
    public static final int DEFAULT_AMOUNT = 1;
    public final AbstractCreature target;
    public final AbstractGameAction.AttackEffect effect;

    // Default constructor for AutoAdd
    public DamageNextTurnPower() {
        this(AbstractDungeon.player);
    }

    public DamageNextTurnPower(AbstractCreature owner) {
        this(owner, DEFAULT_AMOUNT, null, null);
    }

    public DamageNextTurnPower(AbstractCreature owner, int amount, AbstractCreature target, AbstractGameAction.AttackEffect effect) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        this.target = target;
        this.effect = effect;
        updateDescription();
    }

    public DamageNextTurnPower(AbstractCreature owner, int amount, AbstractCreature target, AbstractGameAction.AttackEffect effect, String name) {
        super(NAME, TYPE, TURN_BASED, owner, amount);
        this.target = target;
        this.effect = effect;
        this.name = name;
        updateDescription();
    }

    @Override
    public void updateDescription() {
        description = String.format(DESCRIPTIONS[0],
                amount,
                color_text(target != null ? target.name : "Louse", "#r"));
    }

    @Override
    public void atStartOfTurn() {
        flash();
        atb(new DamageAction(target, new DamageInfo(owner, amount, DamageInfo.DamageType.NORMAL), effect));
        addToBot(new RemoveSpecificPowerAction(owner, owner, this));
    }

    @Override
    public boolean isStackable(AbstractPower power) {
        return power instanceof DamageNextTurnPower
            && ((DamageNextTurnPower)power).target == target
            && power.name.equals(name);
    }
}