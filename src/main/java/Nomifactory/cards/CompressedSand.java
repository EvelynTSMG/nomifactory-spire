package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.MultiCardPreview;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class CompressedSand extends BaseCard {
    public final static String NAME = CompressedSand.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 0;
    public final static int BLOCK = 3;

    public CompressedSand() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        update_card_previews();
        super.initializeDescription();
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        DoubleCompressedSand sand = new DoubleCompressedSand();
        if (upgraded) sand.upgrade();
        make_card_shuffle(sand);
    }

    @Override
    public void upp() {
        update_card_previews();
        updateDescription();
    }

    public void update_card_previews() {
        MultiCardPreview.clear(this);
        DoubleCompressedSand sand = new DoubleCompressedSand();
        Snad snad = new Snad();
        SugarCane cane = new SugarCane();

        if (upgraded) {
            sand.upgrade();
            snad.upgrade();
        }

        MultiCardPreview.add(this, sand, snad, cane);
    }
}