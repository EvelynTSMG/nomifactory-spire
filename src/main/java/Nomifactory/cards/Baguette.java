package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.ChokePower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class Baguette extends BaseCard {
    public final static String NAME = Baguette.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 1;
    public final static int DMG = 6;
    public final static int UP_DMG = 3;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public Baguette() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.BLUNT_LIGHT);
        apply_enemy(m, new ChokePower(m, magicNumber));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}