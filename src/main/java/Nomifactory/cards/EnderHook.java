package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class EnderHook extends BaseCard {
    public final static String NAME = EnderHook.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF_AND_ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;

    public EnderHook() {
        super(NAME, COST, TYPE, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

    }

    @Override
    public void upp() {
    }
}