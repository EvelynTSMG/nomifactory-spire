package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.actions.common.GainEnergyAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.EnergizedBluePower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class Snad extends BaseCard {
    public final static String NAME = Snad.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.SPECIAL;

    public final static int COST = 1;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;
    public final static int MAGIC_2 = 2;

    public Snad() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC_2);
        exhaust = true;
        cardsToPreview = new SugarCane();
        super.initializeDescription();
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        make_card_hand(new SugarCane(), magicNumber);
        apply_self(new EnergizedBluePower(p, magic2));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}