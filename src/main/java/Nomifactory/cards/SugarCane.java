package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class SugarCane extends BaseCard {
    public final static String NAME = SugarCane.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 0;
    public final static int DMG = 4;
    public final static int UP_DMG = 3;

    public SugarCane() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.SLASH_DIAGONAL);
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        updateDescription();
    }
}