package Nomifactory.cards.machines;

import Nomifactory.actions.InstantAction;
import Nomifactory.cards.BaseCard;
import com.megacrit.cardcrawl.actions.common.UpgradeSpecificCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.util.Wiz.atb;

public abstract class BaseMachine extends BaseCard {
    public static final int MAX_UPGRADES = 8;

    public BaseMachine(final String name, final int cost, final CardType type, final CardRarity rarity, final CardTarget target) {
        super(name, cost, type, rarity, target);
        update_name();
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (timesUpgraded < MAX_UPGRADES) {
            atb(new InstantAction(this::upgrade));
        }
    }

    @Override
    public boolean canUpgrade() {
        return false;
    }

    @Override
    public void upgrade() {
        upgradeName();
        upp();
        update_name();
    }

    public void update_name() {
        String voltage = "?V";

        if (timesUpgraded < 0) {
            voltage = "ULV";
        } else if (timesUpgraded > 8) {
            voltage = "UHV";
        } else {
            switch (timesUpgraded) {
                case 0: voltage = "LV"; break;
                case 1: voltage = "MV"; break;
                case 2: voltage = "HV"; break;
                case 3: voltage = "EV"; break;
                case 4: voltage = "IV"; break;
                case 5: voltage = "LuV"; break;
                case 6: voltage = "ZPM"; break;
                case 7: voltage = "UV"; break;
                case 8: voltage = "UHV"; break;
            }
        }

        name = voltage + " " + originalName;
        initializeTitle();
    }
}
