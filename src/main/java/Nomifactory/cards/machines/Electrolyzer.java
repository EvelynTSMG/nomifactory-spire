package Nomifactory.cards.machines;

import Nomifactory.actions.CompareAction;
import Nomifactory.actions.InstantAction;
import Nomifactory.actions.SequenceAction;
import basemod.cardmods.ExhaustMod;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ExhaustAction;
import com.megacrit.cardcrawl.actions.utility.NewQueueCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardQueueItem;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class Electrolyzer extends BaseMachine {
    public final static String NAME = Electrolyzer.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 2;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public Electrolyzer() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (magicNumber <= 0) {
            atb(new ExhaustAction(1, false, false, false));
            super.use(p, m);
            return;
        }

        atb(new SequenceAction(
            new ExhaustAction(1, false, false, false),
            new InstantAction(() -> {
                if (AbstractDungeon.handCardSelectScreen.selectedCards.isEmpty()) return;

                AbstractCard exhaustedCard = AbstractDungeon.handCardSelectScreen.selectedCards.getBottomCard();

                for (int i = 0; i < magicNumber; i++) {
                    AbstractCard copy = exhaustedCard.makeSameInstanceOf();
                    copy.purgeOnUse = true;

                    att(new NewQueueCardAction(copy, true, true, true));
                }
            })
        ));

        super.use(p, m);
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}