package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import Nomifactory.cards.ultimate.BaseUltimateCard;
import com.evacipated.cardcrawl.mod.stslib.fields.cards.AbstractCard.PurgeField;
import com.evacipated.cardcrawl.modthespire.lib.SpireField;
import com.megacrit.cardcrawl.actions.unique.AddCardToDeckAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.Optional;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class HeartOfTheUniverse extends BaseCard {
    public final static String NAME = HeartOfTheUniverse.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = 4;

    public HeartOfTheUniverse() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        PurgeField.purge.set(this, true);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        Optional<AbstractCard> ultimate = random_card((c) -> c instanceof BaseUltimateCard, true);
        ultimate.ifPresent(c -> atb(new AddCardToDeckAction(c)));
    }

    @Override
    public void upp() { }
}