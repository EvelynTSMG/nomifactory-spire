package Nomifactory.cards.ultimate;

import Nomifactory.actions.BranchingAction;
import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.MetallicizePower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class UltimateChestplate extends BaseUltimateCard {
    public final static String NAME = UltimateChestplate.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -2;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public UltimateChestplate() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void triggerOnManualDiscard() {
        atb(new BranchingAction(
                () -> player().currentBlock == 0,
                new ApplyPowerAction(player(), player(), new MetallicizePower(player(), magicNumber))
        ));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}