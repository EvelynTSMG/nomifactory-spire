package Nomifactory.cards.ultimate;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.DexterityPower;
import com.megacrit.cardcrawl.powers.LoseDexterityPower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class UltimateBoots extends BaseUltimateCard {
    public final static String NAME = UltimateBoots.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -2;
    public final static int MAGIC = 5;
    public final static int UP_MAGIC = 2;

    public UltimateBoots() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void triggerWhenDrawn() {
        super.triggerWhenDrawn();
        apply_self(new DexterityPower(player(), magicNumber));
        apply_self(new LoseDexterityPower(player(), magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
    }
}