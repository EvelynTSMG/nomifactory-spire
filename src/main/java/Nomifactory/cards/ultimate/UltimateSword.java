package Nomifactory.cards.ultimate;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DamageRandomEnemyAction;
import com.megacrit.cardcrawl.actions.common.DiscardSpecificCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class UltimateSword extends BaseUltimateCard {
    public final static String NAME = UltimateSword.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -2;
    public final static int MAGIC = 8;
    public final static int UP_MAGIC = 6;

    public UltimateSword() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void triggerOnOtherCardPlayed(AbstractCard c) {
        if (c.type == CardType.ATTACK) {
            atb(new DamageRandomEnemyAction(
                    new DamageInfo(player(), magicNumber, DamageInfo.DamageType.THORNS),
                    AbstractGameAction.AttackEffect.SLASH_HEAVY));
            atb(new DiscardSpecificCardAction(this));
        }
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}