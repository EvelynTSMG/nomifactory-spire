package Nomifactory.cards.ultimate;

import Nomifactory.cards.BaseCard;

import Nomifactory.cards.hooks.OnPlayerDamagedCard;
import com.megacrit.cardcrawl.actions.common.DiscardSpecificCardAction;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class UltimateHelmet extends BaseUltimateCard implements OnPlayerDamagedCard {
    public final static String NAME = UltimateHelmet.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.NONE;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -2;
    public final static int MAGIC = 1;
    public final static int UP_MAGIC = 1;

    public UltimateHelmet() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        selfRetain = true;
    }

    @Override
    public int on_player_damaged(int amount, DamageInfo info) {
        return Math.max(0, amount - magicNumber);
    }

    @Override
    public void atTurnStartPreDraw() {
        atb(new DiscardSpecificCardAction(this));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}