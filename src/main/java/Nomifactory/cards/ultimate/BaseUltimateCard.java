package Nomifactory.cards.ultimate;

import Nomifactory.NomifactoryMod;
import Nomifactory.actions.WinCombatAction;
import Nomifactory.cards.BaseCard;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.util.Wiz.att;
import static Nomifactory.util.Wiz.player;

public abstract class BaseUltimateCard extends BaseCard {
    public BaseUltimateCard(final String name, final int cost, final CardType type, final CardRarity rarity, final CardTarget target) {
        super(name, cost, type, rarity, target);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) { }

    @Override
    public boolean canUse(AbstractPlayer p, AbstractMonster m) {
        return false;
    }

    @Override
    public boolean canUpgrade() {
        return false;
    }

    @Override
    public void triggerWhenDrawn() {
        boolean has_helmet = cardID.equals(UltimateHelmet.ID);
        boolean has_chestplate = cardID.equals(UltimateChestplate.ID);
        boolean has_leggings = cardID.equals(UltimateLeggings.ID);
        boolean has_boots = cardID.equals(UltimateBoots.ID);
        boolean has_sword = cardID.equals(UltimateSword.ID);
        for (AbstractCard c : player().hand.group) {
            has_helmet |= c.cardID.equals(UltimateHelmet.ID);
            has_chestplate |= c.cardID.equals(UltimateChestplate.ID);
            has_leggings |= c.cardID.equals(UltimateLeggings.ID);
            has_boots |= c.cardID.equals(UltimateBoots.ID);
            has_sword |= c.cardID.equals(UltimateSword.ID);
        }

        if (has_helmet && has_chestplate && has_leggings && has_boots && has_sword) {
            att(new WinCombatAction());
        }
    }
}
