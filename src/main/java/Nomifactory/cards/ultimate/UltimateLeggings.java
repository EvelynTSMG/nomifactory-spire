package Nomifactory.cards.ultimate;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.actions.common.ApplyPowerToRandomEnemyAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class UltimateLeggings extends BaseUltimateCard {
    public final static String NAME = UltimateLeggings.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.ALL_ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -2;
    public final static int MAGIC = 2;
    public final static int UP_MAGIC = 1;

    public UltimateLeggings() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void triggerOnEndOfPlayerTurn() {
        atb(new ApplyPowerToRandomEnemyAction(player(), new VulnerablePower(null, magicNumber, false), magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}