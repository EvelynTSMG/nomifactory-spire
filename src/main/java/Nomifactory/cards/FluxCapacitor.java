package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.EnergizedBluePower;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class FluxCapacitor extends BaseCard {
    public final static String NAME = FluxCapacitor.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.COMMON;

    public final static int COST = 0;
    public final static int BLOCK = 4;
    public final static int UP_BLOCK = 3;
    public final static int MAGIC = 1;

    public FluxCapacitor() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        apply_self(new EnergizedBluePower(p, magicNumber));
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
        updateDescription();
    }
}