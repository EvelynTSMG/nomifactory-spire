package Nomifactory.cards.hooks;

import com.megacrit.cardcrawl.cards.DamageInfo;

public interface OnPlayerDamagedCard {
    int on_player_damaged(int amount, DamageInfo info);
}
