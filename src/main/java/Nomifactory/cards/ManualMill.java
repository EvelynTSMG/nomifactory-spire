package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import Nomifactory.powers.DamageNextTurnPower;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class ManualMill extends BaseCard {
    public final static String NAME = ManualMill.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 1;
    public final static int DMG = 4;
    public final static int UP_DMG = 2;

    public ManualMill() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        damage(m, AbstractGameAction.AttackEffect.BLUNT_LIGHT);
        apply_self(new DamageNextTurnPower(p, damage, m, AbstractGameAction.AttackEffect.BLUNT_LIGHT, name));
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
    }
}