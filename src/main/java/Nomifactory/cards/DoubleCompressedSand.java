package Nomifactory.cards;

import Nomifactory.cards.BaseCard;

import basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.MultiCardPreview;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static Nomifactory.NomifactoryMod.id;
import static Nomifactory.util.Wiz.*;

public class DoubleCompressedSand extends BaseCard {
    public final static String NAME = DoubleCompressedSand.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.SPECIAL;

    public final static int COST = 1;
    public final static int BLOCK = 6;

    public DoubleCompressedSand() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
        update_card_previews();
        super.initializeDescription();
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
        Snad snad = new Snad();
        if (upgraded) snad.upgrade();
        make_card_hand(snad);
    }

    @Override
    public void upp() {
        update_card_previews();
        updateDescription();
    }

    public void update_card_previews() {
        MultiCardPreview.clear(this);
        Snad snad = new Snad();
        SugarCane cane = new SugarCane();

        if (upgraded) {
            snad.upgrade();
        }

        MultiCardPreview.add(this, snad, cane);
    }
}